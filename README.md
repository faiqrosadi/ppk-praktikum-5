FAIQ ROSADI ARRIDHO <br>
222011636 <br>
3SI3 <br>

Dokumentasi Kegiatan Praktikum: 

<b>Method Post Register</b><br>
![](/Documentation/register.png)

<b>Method Post Login</b><br>
![](/Documentation/login.png)

<b>Method Post Me</b><br>
![](/Documentation/akses%20me.png)

<b>Akses Home with no token</b><br>
![](/Documentation/akses%20home%20(no%20token).png)

<b>Akses Home with invalid token</b><br>
![](/Documentation/akses%20home%20(invalid%20token).png)

<b>Akses Home with valid token</b><br>
![](/Documentation/akses%20home%20(valid%20token).png)
